<?php

namespace Tests\Unit;

use App\Models\User;
use App\Models\Project;
use App\Models\Social;
use App\Models\Meta;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

class UserTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function finding_all_the_users_projects()
    {
    	$linus = factory(User::class)->create();

    	$projects = new EloquentCollection(
    		$linus->projects()
			->saveMany(factory(Project::class, 5)->create())
    	);

    	$foundProjects = $linus->projects;

    	$projects->assertEquals($foundProjects);
    }


	/** @test */
	public function finding_all_the_users_social_handles()
	{
		$linus = factory(User::class)->create();

		$socialCollection = new EloquentCollection(
			$linus->socials()
			->saveMany(factory(Social::class, 5)->create())
		);

		$socialCollection->assertEquals($linus->socials);
	}

	/** @test */
	public function finding_all_the_users_metas()
	{
		$linus = factory(User::class)->create();

		$faker = \Faker\Factory::create();

		$socialCollection = new EloquentCollection(
			$linus->metas()
			->saveMany(factory(Meta::class, 5)->create([
				'meta_key' => 'first_name',
				'meta_value' => $faker->firstName
			]))
		);

		$socialCollection->assertEquals($linus->metas);
	}
}
