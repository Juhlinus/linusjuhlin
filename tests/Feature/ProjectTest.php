<?php

namespace Tests\Feature;

use App\Models\Project;
use App\Models\User;

use Tests\TestCase;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;

class ProjectTest extends TestCase
{
	use RefreshDatabase;
	use WithFaker;

    /** @test */
    public function user_should_have_multiple_projects()
    {
    	$linus = factory(User::class)->create();

		$projectCollection = new EloquentCollection(
			$linus->projects()
			->saveMany(factory(Project::class, 5)->create())
		);

		$projectCollection->assertEquals($linus->projects);
    }

    /** @test */
	public function all_projects_should_be_passed_to_the_view()
	{
		$faker = \Faker\Factory::create();

		$linus = factory(User::class)->create();

		$projectCollection = new EloquentCollection([
			$linus->projects()->save(factory(Project::class)->create(['url' => $faker->url, 'name' => $faker->name])),
			$linus->projects()->save(factory(Project::class)->create(['url' => $faker->url, 'name' => $faker->name])),
			$linus->projects()->save(factory(Project::class)->create(['url' => $faker->url, 'name' => $faker->name])),
			$linus->projects()->save(factory(Project::class)->create(['url' => $faker->url, 'name' => $faker->name])),
			$linus->projects()->save(factory(Project::class)->create(['url' => $faker->url, 'name' => $faker->name]))
		]);

		$response = $this->withoutExceptionHandling()->get('/');

		$response->assertSuccessful();

		$projectCollection->assertEquals($response->data('projects'));
	}

	/** @test */
	// public function only_the_top_five_projects_are_shown_in_descending_order_by_date()
	// {
	// 	$faker = \Faker\Factory::create();
		
	// 	$linus = factory(User::class)->create();

	// 	$projectCollection = new EloquentCollection([
	// 		$linus->projects()->save(factory(Project::class)->create(['created_at' => $date = Carbon::now()->addDays($faker->randomDigit), 'updated_at' => $date])),
	// 		$linus->projects()->save(factory(Project::class)->create(['created_at' => $date = Carbon::now()->addDays($faker->randomDigit), 'updated_at' => $date])),
	// 		$linus->projects()->save(factory(Project::class)->create(['created_at' => $date = Carbon::now()->addDays($faker->randomDigit), 'updated_at' => $date])),
	// 		$linus->projects()->save(factory(Project::class)->create(['created_at' => $date = Carbon::now()->addDays($faker->randomDigit), 'updated_at' => $date])),
	// 		$linus->projects()->save(factory(Project::class)->create(['created_at' => $date = Carbon::now()->addDays($faker->randomDigit), 'updated_at' => $date])),
	// 		$linus->projects()->save(factory(Project::class)->create(['created_at' => $date = Carbon::now()->addDays($faker->randomDigit), 'updated_at' => $date])),
	// 	]);

	// 	$sortedCollection = $projectCollection->sortByDesc('created_at');

	// 	$sortedCollection->forget($sortedCollection->keys()->last());

	// 	$response = $this->withoutExceptionHandling()->get('/projects');
	// 	$response->assertSuccessful();

	// 	$sortedCollection->assertEquals($response->data('projects'));
	// }

	/** @test */
	// public function when_no_projects_exist_then_a_placeholder_text_shown()
	// {
	// 	$linus = factory(User::class)->create();

	// 	$response = $this->withoutExceptionHandling()->get('/projects');

	// 	$response->assertSee('No projects to show');
	// }
}