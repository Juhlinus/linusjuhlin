<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Meta;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

class EditProfileTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

	/** @test */
    public function guest_cant_view_the_profile_information_page()
    {
		$this->expectException(\Illuminate\Auth\AuthenticationException::class);

		$response = $this->withoutExceptionHandling()->get('edit_profile/');
		$response->assertRedirect('login/')
			->assertSuccessful();
    }

    /** @test */
    public function user_can_view_the_profile_information_page()
    {
    	$user = factory(User::class)->create();

		$response = $this->withoutExceptionHandling()->actingAs($user)->get('edit_profile/');
		$response->assertSuccessful();
    }

    /** @test */
    public function all_profile_information_is_passed_to_the_edit_profile_view()
    {
    	$linus = factory(User::class)->create();

		$userMetaCollection = new EloquentCollection([
			$linus->metas()->save(factory(Meta::class)->create(['meta_key' => 'first_name', 'meta_value' => 'Linus'])),
			$linus->metas()->save(factory(Meta::class)->create(['meta_key' => 'last_name', 'meta_value' => 'Juhlin'])),
			$linus->metas()->save(factory(Meta::class)->create(['meta_key' => 'description', 'meta_value' => 'Hello my name is Linus'])),
			$linus->metas()->save(factory(Meta::class)->create(['meta_key' => 'profile_picture', 'meta_value' => 'avatars/avatar.jpg'])),
		]);

		$response = $this->withoutExceptionHandling()->actingAs($linus)->get('edit_profile/');
		$response->assertSuccessful();

		$userMetaCollection->assertEquals($response->data('user')->metas);
    }

   //  /** @test */
   //  public function after_submitting_new_profile_information_it_is_stored_in_the_database_and_on_disk()
   //  {
   //  	Storage::fake('avatars');

   //  	$firstName = $this->faker()->firstName;
   //  	$lastName = $this->faker()->lastName;
   //  	$description = $this->faker()->text;
   //  	$profile_picture = UploadedFile::fake()->image('avatar.jpg');

   //  	$user = factory(User::class)->create();

   //  	$response = $this->withoutExceptionHandling()->actingAs($user)->put('/edit_profile', [
   //  		'first_name' 		=> $firstName,
   //  		'last_name' 		=> $lastName,
   //  		'description' 		=> $description,
   //  		'avatar' 	        => $profile_picture,
   //  	]);

   //  	$this->assertDatabaseHas('metas', [
   //  		'meta_key' => 	'first_name',
			// 'meta_key' => 	'last_name',
			// 'meta_key' => 	'description',
			// 'meta_key' => 	'avatar',
   //  		'meta_value' => $firstName,
   //  		'meta_value' => $lastName,
   //  		'meta_value' => $description,
   //  		'meta_value' => 'public/avatars/' . $profile_picture->getClientOriginalName(),
   //  	]);
   //  }
}
