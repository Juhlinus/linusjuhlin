<?php

namespace Tests\Feature;

use App\Models\Meta;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

class ProfileTest extends TestCase
{
	use RefreshDatabase;
	use WithFaker;

    /** @test */
    public function all_profile_information_is_passed_to_the_home_view()
    {
    	$linus = factory(User::class)->create();

		$userMetaCollection = new EloquentCollection([
			$linus->metas()->save(factory(Meta::class)->create(['meta_key' => 'first_name', 'meta_value' => 'Linus'])),
			$linus->metas()->save(factory(Meta::class)->create(['meta_key' => 'last_name', 'meta_value' => 'Juhlin'])),
			$linus->metas()->save(factory(Meta::class)->create(['meta_key' => 'description', 'meta_value' => 'Hello my name is Linus'])),
			$linus->metas()->save(factory(Meta::class)->create(['meta_key' => 'profile_picture', 'meta_value' => 'avatars/avatar.jpg'])),
		]);

        $userMetaCollection = $userMetaCollection->flatMap(function ($item, $key) {
            return [$item->meta_key => $item->meta_value];
        });

        $response = $this->withoutExceptionHandling()->get('/');
        $response->assertSuccessful();

        $this->assertEquals($userMetaCollection, $response->data('metas'));
    }
}
