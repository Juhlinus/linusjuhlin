<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Social;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

// If no socials exist
class SocialTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function all_social_handles_should_be_passed_to_the_view()
	{
		$faker = \Faker\Factory::create();

		$linus = factory(User::class)->create();

		$socialCollection = new EloquentCollection([
			$linus->socials()->save(factory(Social::class)->create()),
			$linus->socials()->save(factory(Social::class)->create()),
			$linus->socials()->save(factory(Social::class)->create()),
			$linus->socials()->save(factory(Social::class)->create()),
			$linus->socials()->save(factory(Social::class)->create())
		]);

		$response = $this->withoutExceptionHandling()->get('/');
		$response->assertSuccessful();

		$socialCollection->assertEquals($response->data('socials'));
	}

	/** @test */
	public function anyone_should_be_able_to_see_the_social_handles()
	{
		$linus = factory(User::class)->create();

		$socialCollection = new EloquentCollection(
			$linus->socials()
			->saveMany(factory(Social::class, 5)->create())
		);

		$response = $this->withoutExceptionHandling()->get('/');
		$response->assertSuccessful();

		$socialCollection->each(function ($social) use ($response) {
			$response->assertSeeInOrder([$social->url, $social->name]);
		});
	}

	/** @test */
	public function when_no_social_handles_exist_then_a_placeholder_text_shown()
	{
		$linus = factory(User::class)->create();

		$response = $this->withoutExceptionHandling()->get('/');

		$response->assertSee('No social handles to show');
	}
}
