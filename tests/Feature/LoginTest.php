<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
	public function a_logged_in_user_should_be_sent_to_the_edit_profile_page()
	{
		$user = factory(User::class)->create();

		$response = $this->withoutExceptionHandling()
			->actingAs($user)
			->get('login/');
		
		$response->assertRedirect('edit_profile/');
	}

    /** @test */
	public function a_guest_should_be_able_to_view_the_login_page()
	{
		$response = $this->withoutExceptionHandling()
			->get('login/');
		
		$response->assertSuccessful();
	}
}
