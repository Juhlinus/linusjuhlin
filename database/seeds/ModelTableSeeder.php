<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class ModelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(\App\Models\User::class)->create([
            'name' => 'Linus Juhlin',
            'email' => 'me@linusjuhlin.me',
            'password' => bcrypt(env('USER_PASSWORD')),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ])->each(function ($user) {
            // Metas
            $user->metas()->save(factory(\App\Models\Meta::class)->create(['meta_key' => 'first_name', 'meta_value' => 'Linus']));
            $user->metas()->save(factory(\App\Models\Meta::class)->create(['meta_key' => 'last_name', 'meta_value' => 'Juhlin']));
            $user->metas()->save(factory(\App\Models\Meta::class)->create(['meta_key' => 'description', 'meta_value' => 'Hello, my name is Linus Juhlin. 
                I\'m an enthusiastic software developer currently residing in Tokyo, Japan.']));
            $user->metas()->save(factory(\App\Models\Meta::class)->create(['meta_key' => 'avatar', 'meta_value' => 'public/avatars/avatar.jpg']));

            // Projects
            $user->projects()->save(factory(\App\Models\Project::class)->create(['url' => 'https://gitlab.com/Juhlinus/linusjuhlin', 'name' => 'Portfolio Website']));
            $user->projects()->save(factory(\App\Models\Project::class)->create(['url' => 'https://github.com/Juhlinus/SabberStone', 'name' => 'SabberStone']));
            $user->projects()->save(factory(\App\Models\Project::class)->create(['url' => 'https://github.com/Juhlinus/Nez', 'name' => 'Nez']));
            $user->projects()->save(factory(\App\Models\Project::class)->create(['url' => 'https://github.com/Juhlinus/nez-pong', 'name' => 'Pong in Nez (C#)'])); 
            
            // Social Handles
            $user->socials()->save(factory(\App\Models\Social::class)->create(['url' => 'https://github.com/juhlinus/', 'name' => 'GitHub']));
            $user->socials()->save(factory(\App\Models\Social::class)->create(['url' => 'https://twitter.com/juhlinus/', 'name' => 'Twitter']));
            $user->socials()->save(factory(\App\Models\Social::class)->create(['url' => 'https://linkedin.com/in/juhlinus/', 'name' => 'LinkedIn'])); 
        });
    }
}
