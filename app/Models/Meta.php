<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
	protected $fillable = ['meta_key', 'meta_value'];

    public function user()
    {
    	return $this->belongsToMany(User::class);
    }
}
