<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function socials()
    {
        return $this->belongsToMany(Social::class);
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }

    public function metas()
    {
        return $this->belongsToMany(Meta::class);
    }

    public function projectsInDescendingOrder($limit = 5)
    {
        return $this->projects()->orderBy('created_at', 'desc')->limit($limit);
    }
}
