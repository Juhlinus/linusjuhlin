<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['portfolio.index'], function($view)
        {
            $view->with('metas', \App\Models\User::first()->metas->flatMap(function ($item, $key) {
                    return [$item->meta_key => $item->meta_value];
                })
            );
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}