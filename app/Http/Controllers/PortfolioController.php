<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function index()
    {
    	$linus = User::all()->first();

    	$socials = $linus->socials;
    	$projects = $linus->projects;

		return view('portfolio.index', compact(
			'socials', 
			'projects'
		));
    }
}
