<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index()
    {
    	$linus = User::all()->first();

    	$projects = $linus->projectsInDescendingOrder()->get();

    	return view('projects.index', compact('projects'));
    }
}
