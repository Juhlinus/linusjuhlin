<?php

namespace App\Http\Controllers;

use App\Models\Meta;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class EditProfileController extends Controller
{
	public function index()
	{
		$user = Auth::user();

		return view('profile.index', compact('user'));
	}

	public function update(Request $request)
	{
		$request->avatar = $request->file('avatar')->storeAs(env('FILESYSTEM_DRIVER'), 'avatars/avatar.jpg');

		$user = Auth::user();

		$user->metas()->get()->each(function ($meta) use ($request) {

			$meta_key = $meta->meta_key;

			$meta->updateOrCreate(
				['meta_key' => $meta_key],
				['meta_value' => $request->$meta_key]
			);

		});

		return Redirect::to('edit_profile/')->with('message', 'Updated data information successfully');
	}
}
