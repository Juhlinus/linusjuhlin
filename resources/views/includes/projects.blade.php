<div class="p-2 bg-red-light text-center justify-center">
	<h1 class="text-5xl py-0 my-0">Projects</h1>
	<div class="p-2">
	@forelse ($projects as $project)
		<a class="sm:inline sm:text-justify py-2 no-underline hover:underline text-grey-lighter text-shadow text-3xl px-4 block text-center" href="{{ $project->url }}">{{ $project->name }}</a>
	@empty
		<p>No project to show.</p>
	@endforelse
	</div>
</div>
