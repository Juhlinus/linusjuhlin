<div class="p-6 pb-0 text-center justify-center">
	<h1 class="text-5xl py-0 my-0 text-shadow">Social Handles</h1>
	<div class="p-2 pb-0">
	@forelse ($socials as $social)
		<a class="sm:inline sm:text-justify py-2 no-underline hover:underline text-grey-lighter text-shadow text-3xl px-4 block text-center" href="{{ $social->url }}">{{ $social->name }}</a>
	@empty
		<p>No social handles to show.</p>
	@endforelse
	</div>
</div>