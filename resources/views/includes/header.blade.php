    <header class="w-full text-center border-t-4 border-red-light p-4">
        <img src="{{ asset('storage/' . $metas->get('avatar')) }}" alt="profile picture" class="rounded-full shadow-md w-32" />
    </header>