<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
  <body class="flex flex-col h-full bg-red-light">
    
    <div id="content" class="flex-1 mx-auto pb-4 w-full text-white bg-blue-light">
        @yield('content')
    </div>

    <footer class="w-full pin-b text-center bg-red-light p-6 mt-2 h-16 text-lg text-white">
        @include('includes.footer')
    </footer>

  </body>
</html>