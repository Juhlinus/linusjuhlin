@extends('layouts.app')

@section('body')
	@forelse ($projects as $project)
		{{ $project }}
	@empty
		No projects to show.
	@endforelse
@endsection
