@extends('layouts.app')

@section('content')
	{{-- Logout --}}
	<form method="post" action="{{ route('logout') }}">
		@csrf
		<button type="submit">Logout</button>
	</form>

    <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 max-w-xs m-auto mt-6" method="POST" action="{{ route('edit_profile') }}" aria-label="{{ __('Edit Profile') }}" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="mb-4">
            <label class="block text-grey-darker text-sm font-bold mb-2" for="first_name">
                First Name
            </label>
            <input id="first_name" type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight" name="first_name" value="{{ old('first_name') }}" placeholder="First Name" required autofocus>
            @if ($errors->has('first_name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
            @endif
        </div>
        <div class="mb-4">
            <label class="block text-grey-darker text-sm font-bold mb-2" for="last_name">
                Last Name
            </label>
            <input id="last_name" type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name" required autofocus>
            @if ($errors->has('last_name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
            @endif
        </div>
        <div class="mb-4">
            <label class="block text-grey-darker text-sm font-bold mb-2" for="description">
                Description
            </label>
            <textarea id="description" type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight" name="description" value="{{ old('description') }}" placeholder="Description" required autofocus></textarea>
            @if ($errors->has('description'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
        <div class="mb-6">
            <label class="block text-grey-darker text-sm font-bold mb-2" for="avatar">
                Avatar
            </label>
            <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 w-full inline-flex items-center justify-center rounded">
            	<svg fill="#FFF" height="18" viewBox="0 0 24 24" width="18" xmlns="http://www.w3.org/2000/svg">
            		<path d="M0 0h24v24H0z" fill="none"/>
            		<path d="M9 16h6v-6h4l-7-7-7 7h4zm-4 2h14v2H5z"/>
            	</svg>
            	<span class="ml-2">Browse</span>
            </button>
            <input type="file" name="avatar" class="cursor-pointer relative block opacity-0 file-t file-r">
            @if ($errors->has('avatar'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('avatar') }}</strong>
            </span>
            @endif
        </div>
        <div class="flex items-center justify-center">
            <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded" type="submit">
                Update
            </button>
        </div>
    </form>


@endsection