@extends('layouts.app')

@section('content')
	@include('includes.header')

	<div id="content" class="flex-1 mx-auto pb-4 w-full text-white">

		<div class="font-sans-condensed leading-none text-center leading-normal">
			<h1 class="text-5xl text-grey-lightest">{{ $metas->get('first_name') . ' ' . $metas->get('last_name') }}</h1>
			<p class="pt-4 pb-8 text-grey-lighter text-lg">{!! nl2br(e($metas->get('description'))) !!}</p>	
		</div>
		@include('includes.projects')
		@include('includes.social')

	</div>
@endsection